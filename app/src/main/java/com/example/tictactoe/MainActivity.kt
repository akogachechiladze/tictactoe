package com.example.tictactoe

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Message
import android.view.View
import android.widget.Button
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {

    private var isPlayerOne = true



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
        Restart()
    }
    private fun init() {
     Button00.setOnClickListener {
         buttonChange(Button00)

     }
     Button01.setOnClickListener {
         buttonChange(Button01)

     }
     Button02.setOnClickListener {
         buttonChange(Button02)

     }
     Button10.setOnClickListener {
         buttonChange(Button10)

     }
     Button11.setOnClickListener {
         buttonChange(Button11)

     }
     Button12.setOnClickListener {
         buttonChange(Button12)

     }
     Button20.setOnClickListener {
         buttonChange(Button20)

     }
     Button21.setOnClickListener {
         buttonChange(Button21)

     }
     Button22.setOnClickListener {
         buttonChange(Button22)

     }
    }

    override fun onClick(v: View?) {

    }

    private fun buttonChange(button: Button){
        if (isPlayerOne){
            button.text = "x"
        }else {
            button.text = "o"
        }
        button.isClickable = false
        isPlayerOne = !isPlayerOne
        checkWinner()

    }

    private fun checkWinner() {
        if (Button00.text.toString().isNotEmpty() && Button00.text.toString() == Button01.text.toString() && Button01.text.toString() == Button02.text.toString()) {
            showToast(Button00.text.toString())

        } else if (Button10.text.toString().isNotEmpty() && Button10.text.toString() == Button11.text.toString() && Button11.text.toString() == Button12.text.toString()) {
            showToast(Button10.text.toString())
            EndGame()
        } else if (Button20.text.toString().isNotEmpty() && Button20.text.toString() == Button11.text.toString() && Button21.text.toString() == Button22.text.toString()) {
            showToast(Button20.text.toString())
            EndGame()
        } else if (Button00.text.toString().isNotEmpty() && Button00.text.toString() == Button10.text.toString() && Button10.text.toString() == Button20.text.toString()) {
            showToast(Button00.text.toString())
            EndGame()
        } else if (Button01.text.toString().isNotEmpty() && Button01.text.toString() == Button11.text.toString() && Button11.text.toString() == Button21.text.toString()) {
            showToast(Button01.text.toString())
            EndGame()
        }else if (Button02.text.toString().isNotEmpty() && Button02.text.toString() == Button12.text.toString() && Button12.text.toString() == Button22.text.toString()) {
            showToast(Button02.text.toString())
            EndGame()
        }else if (Button00.text.toString().isNotEmpty() && Button00.text.toString() == Button11.text.toString() && Button11.text.toString() == Button22.text.toString()) {
            showToast(Button00.text.toString())
            EndGame()
        }else if (Button02.text.toString().isNotEmpty() && Button02.text.toString() == Button11.text.toString() && Button11.text.toString() == Button20.text.toString()){
            showToast(Button02.text.toString())
            EndGame()
        }else if (Button00.text.toString().isNotEmpty() && Button01.text.toString().isNotEmpty() && Button02.text.toString().isNotEmpty() && Button10.text.toString().isNotEmpty() && Button11.text.toString().isNotEmpty() && Button12.text.toString().isNotEmpty() && Button20.text.toString().isNotEmpty() && Button21.text.isNotEmpty() && Button22.text.toString().isNotEmpty())
            Toast.makeText(this, "No winner",Toast.LENGTH_SHORT).show()
    }
    private fun showToast(message:String){
        Toast.makeText(this, "Winner is $message",Toast.LENGTH_SHORT).show()
    }
    private fun Restart(){
        RestartButton.setOnClickListener {
            Button00.text=""
            Button01.text=""
            Button02.text=""
            Button10.text=""
            Button11.text=""
            Button12.text=""
            Button20.text=""
            Button21.text=""
            Button22.text=""
            Button00.isClickable=true
            Button01.isClickable=true
            Button02.isClickable=true
            Button10.isClickable=true
            Button11.isClickable=true
            Button12.isClickable=true
            Button20.isClickable=true
            Button21.isClickable=true
            Button22.isClickable=true
        }
    }
    private fun EndGame(){
        Button00.isClickable=false
        Button01.isClickable=false
        Button02.isClickable=false
        Button10.isClickable=false
        Button11.isClickable=false
        Button12.isClickable=false
        Button20.isClickable=false
        Button21.isClickable=false
        Button22.isClickable=false
    }


}